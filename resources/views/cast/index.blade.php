@extends('adminlte.master')

@section('title','Cast Table')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Cast</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>

  <!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header d-flex flex-column">
            <a href="{{ url('/cast/create') }}" class="d-sm-inline-block btn btn-sm btn-success py-2"><i class='fa fa-plus'></i> TAMBAH</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table class="table table-bordered" id="CastTable">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th style="width: 150px">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($cast as $item)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>{{ $item->umur }}</td>
                        <td>{{ $item->bio }}</td>
                        <td>
                            <a href="{{ url("/cast/$item->id") }}" class="btn btn-sm btn-info"><i class='fa fa-eye px-1'></i></a>
                            <a href="{{ url("/cast/$item->id/edit") }}" class="btn btn-sm btn-primary"><i class='fa fa-pen px-1'></i></a>
                            {{-- <a href="{{ url("/cast/$item->id/delete") }}" class="btn btn-sm btn-danger"><i class='fa fa-trash'></i></a> --}}
                            <form action="{{ url("/cast/$item->id") }}" method="POST" class="d-sm-inline">
                                @csrf
                                @method('DELETE')
                                {{-- <input type="submit" class="btn btn-danger my-2" value="Delete"> --}}
                                <button type="submit" class="btn btn-danger btn-sm"><i class='fa fa-trash px-1'></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.card -->

</section>
  <!-- /.content -->

@endsection

@push('script')
    <script>
        $(function () {
            $("#CastTable").DataTable();
        });
    </script>
@endpush
