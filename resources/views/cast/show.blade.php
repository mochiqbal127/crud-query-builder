@extends('adminlte.master')

@section('title','Detail Cast')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Cast</h1>
          <a href="{{ url('/cast') }}" class="d-sm-inline-block btn btn-sm btn-danger py-2">Back</a>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>

  <!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Detail Cast {{$cast->id}}</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td>Nama</td>
                            <td> :</td>
                            <td> {{$cast->nama}}</td>
                        </tr>
                        <tr>
                            <td>Umur </td>
                            <td> :</td>
                            <td> {{$cast->umur}}</td>
                        </tr>
                        <tr>
                            <td>Bio </td>
                            <td> :</td>
                            <td> {{$cast->bio}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
          </div>
    </div>
    <!-- /.card -->

</section>
  <!-- /.content -->

@endsection

