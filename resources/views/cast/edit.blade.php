@extends('adminlte.master')

@section('title','Edit Data')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Cast</h1>
          <a href="{{ url('/cast') }}" class="d-sm-inline-block btn btn-sm btn-danger py-2">Back</a>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>

  <!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Edit Data {{ $cast->id }}</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form class="form-horizontal" action="{{ url("/cast/$cast->id") }}" method="post">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group row">
                    <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="{{ $cast->nama }}" id="nama" name="nama" placeholder="Nama">
                    </div>
                  </div>
                    @error('nama')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $message }}
                    </div>
                    @enderror
                  <div class="form-group row">
                    <label for="umur" class="col-sm-2 col-form-label">Umur</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="{{ $cast->umur }}" id="umur" name="umur" placeholder="Umur">
                    </div>
                  </div>
                    @error('umur')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $message }}
                    </div>
                    @enderror
                  <div class="form-group row">
                    <label for="bio" class="col-sm-2 col-form-label">Bio</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="{{ $cast->bio }}" id="bio" name="bio" placeholder="Bio">
                    </div>
                  </div>
                    @error('bio')
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $message }}
                    </div>
                    @enderror

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
            </form>
          </div>
    </div>
    <!-- /.card -->

</section>
  <!-- /.content -->

@endsection

